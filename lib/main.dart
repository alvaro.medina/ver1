import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:provider/provider.dart';
import 'package:restaurant_app/screens/authentication/welcome.dart';
import 'package:restaurant_app/screens/home/root.dart';
import 'package:restaurant_app/services/auth_services.dart';
import 'package:restaurant_app/services/nav_services.dart';
import 'package:restaurant_app/share_prefs/preferencias_usuario.dart';
import 'package:restaurant_app/theme/theme.dart';
import 'package:restaurant_app/services/firestore_service.dart';
import 'package:firebase_core/firebase_core.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  final prefs = new PreferenciasUsuario();
  await prefs.intiPrefs();
  runApp(
    ChangeNotifierProvider(
        create: (_) => new ThemeChanger(prefs.colorSecundario), child: MyApp()),
  );
}

class MyApp extends StatelessWidget {
  final FirestoreService _db = FirestoreService();
  @override
  Widget build(BuildContext context) {
    //Esto sirve para cambiar el color de arribita del appbar
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Colors.black));

    //final appTheme = Provider.of<ThemeChanger>(context); //-----
    //provider theme
    final currentTheme = Provider.of<ThemeChanger>(context).currentTheme; //----

    return MultiProvider(
      providers: [
        ///Escucha los cambios del estado del login
        ChangeNotifierProvider<AuthenticationService>(
          create: (context) => AuthenticationService(
            FirebaseAuth.instance,
            GoogleSignIn(),
          ),
        ),

        ///Escucha los cambios de estado en el BOTTOM navigator
        ChangeNotifierProvider<NavigationProvider>(
          create: (_) => NavigationProvider(),
        ),
        StreamProvider(
            create: (context) => FirestoreService().getRestaurants(),
            initialData: null),
      ],
      child: AuthWidgetBuilder(
        builder: (BuildContext context, AsyncSnapshot<User> userSnapshot) {
          return MaterialApp(
            title: 'SirvePe',
            theme: currentTheme,
            home: AuthWidget(userSnapshot: userSnapshot),
          );
        },
      ),
    );
  }
}

class AuthWidgetBuilder extends StatelessWidget {
  const AuthWidgetBuilder({Key key, @required this.builder}) : super(key: key);
  final Widget Function(BuildContext, AsyncSnapshot<User>) builder;

  @override
  Widget build(BuildContext context) {
    final authService =
        Provider.of<AuthenticationService>(context, listen: false);

    return StreamBuilder<User>(
      stream: authService.authStateChanges,
      builder: (BuildContext context, AsyncSnapshot<User> snapshot) {
        final User user = snapshot.data;
        if (user != null) {
          return MultiProvider(
            providers: [
              Provider<User>.value(value: user),
              // NOTE: Any other user-bound providers here can be added here
            ],
            child: builder(context, snapshot),
          );
        }
        return builder(context, snapshot);
      },
    );
  }
}

class AuthWidget extends StatelessWidget {
  const AuthWidget({Key key, @required this.userSnapshot}) : super(key: key);
  final AsyncSnapshot<User> userSnapshot;

  @override
  Widget build(BuildContext context) {
    if (userSnapshot.connectionState == ConnectionState.active) {
      return userSnapshot.hasData ? RootPage() : WelcomePage();
    }
    return Scaffold(
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
