import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:restaurant_app/models/food.dart';
import 'package:restaurant_app/models/restaurant.dart';

class FirestoreService {
  //Firestore _db = Firestore.instance;
  FirebaseFirestore _db = FirebaseFirestore.instance;

  /// ----------------Restaurant Section------------------------
  /// -                                                       -
  /// ---------------------------------------------------------
  Stream<List<Restaurant>> getRestaurants() {
    return _db
        .collection('restaurants')
        //.orderBy('timeStamp', descending: true)
        .snapshots()
        .map((snapshot) => snapshot.docs
            .map(
                (document) => Restaurant.fromJson(document.data(), document.id))
            .toList());
    //return _db.collection('apartments').document(); //Gets a future querysnapshot
  }

  //Obtaions restaurants foods from ID 
  Stream<List<Food>> getRestaurantFoods(String id) {
    return _db
        .collection('food')
        .where('restaurant_id', isEqualTo: id)
        .snapshots()
        .map((snapshot) => snapshot.docs
            .map((document) => Food.fromJson(document.data()))
            .toList());


  }
}
