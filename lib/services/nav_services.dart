import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:restaurant_app/models/restaurant.dart';
import 'package:restaurant_app/models/screen.dart';
import 'package:restaurant_app/screens/authentication/welcome.dart';
import 'package:restaurant_app/screens/home/dashboard.dart';
import 'package:restaurant_app/screens/home/payment/food_checklist.dart';
import 'package:restaurant_app/screens/home/principal.dart';
import 'package:restaurant_app/screens/home/profile.dart';
import 'package:restaurant_app/screens/home/restaurant/detail_food.dart';
import 'package:restaurant_app/screens/home/restaurant/detail_page.dart';
import 'package:restaurant_app/screens/home/secondary.dart';

const FIRST_SCREEN = 0;
const SECOND_SCREEN = 1;
const THIRD_SCREEN = 2;

class NavigationProvider extends ChangeNotifier {
  /// Shortcut method for getting [NavigationProvider].
  static NavigationProvider of(BuildContext context) =>
      Provider.of<NavigationProvider>(context, listen: false);

  int _currentScreenIndex = FIRST_SCREEN;

  int get currentTabIndex => _currentScreenIndex;

  final Map<int, Screen> _screens = {
    FIRST_SCREEN: Screen(
      title: 'First',
      child: DashboardPage(),
      initialRoute: DashboardPage.route,
      navigatorState: GlobalKey<NavigatorState>(),
      onGenerateRoute: (settings) {
        print('Generating route: ${settings.name}');
        switch (settings.name) {
          case SecondaryPage.route:
            return MaterialPageRoute(builder: (_) => SecondaryPage());
          case RestaurantDetailPage.route:
            return MaterialPageRoute(builder: (_) => RestaurantDetailPage());
          case DetailFood.route:
            return MaterialPageRoute(builder:(_) => DetailFood());
          case Payment_Checklist.route:
            return MaterialPageRoute(builder: (_) => Payment_Checklist());
          default:
            return MaterialPageRoute(builder: (_) => DashboardPage());
        }
      },
      scrollController: ScrollController(),
    ),
    SECOND_SCREEN: Screen(
      title: 'Second',
      child: ProfilePage(),
      initialRoute: ProfilePage.route,
      navigatorState: GlobalKey<NavigatorState>(),
      onGenerateRoute: (settings) {
        print('Generating route: ${settings.name}');
        switch (settings.name) {
          default:
            return MaterialPageRoute(builder: (_) => ProfilePage());
        }
      },
      scrollController: ScrollController(),
    ),
    THIRD_SCREEN: Screen(
      title: 'Third',
      child: PrincipalPage(),
      initialRoute: PrincipalPage.route,
      navigatorState: GlobalKey<NavigatorState>(),
      onGenerateRoute: (settings) {
        print('Generating route: ${settings.name}');
        switch (settings.name) {
          default:
            return MaterialPageRoute(builder: (_) => PrincipalPage());
        }
      },
      scrollController: ScrollController(),
    ),
  };

  List<Screen> get screens => _screens.values.toList();

  Screen get currentScreen => _screens[_currentScreenIndex];

  /// Set currently visible tab.
  void setTab(int tab) {
    if (tab == currentTabIndex) {
      _scrollToStart();
    } else {
      _currentScreenIndex = tab;
      notifyListeners();
    }
  }

  /// If currently displayed screen has given [ScrollController] animate it
  /// to the start of scroll view.
  void _scrollToStart() {
    if (currentScreen.scrollController.hasClients) {
      currentScreen.scrollController.animateTo(
        0,
        duration: const Duration(seconds: 1),
        curve: Curves.easeInOut,
      );
    }
  }

  /// Provide this to [WillPopScope] callback.
  Future<bool> onWillPop(BuildContext context) async {
    final currentNavigatorState = currentScreen.navigatorState.currentState;

    if (currentNavigatorState.canPop()) {
      currentNavigatorState.pop();
      return false;
    } else {
      if (currentTabIndex != FIRST_SCREEN) {
        setTab(FIRST_SCREEN);
        notifyListeners();
        return false;
      } else {
        return await showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Exit?'),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
                child: Text(
                  'Cancel',
                  style: Theme.of(context).textTheme.button.copyWith(
                        fontWeight: FontWeight.normal,
                      ),
                ),
              ),
              FlatButton(
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
                child: Text('Exit'),
              ),
            ],
          ),
        );
      }
    }
  }
}
