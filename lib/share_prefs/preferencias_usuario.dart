import 'package:shared_preferences/shared_preferences.dart';

class PreferenciasUsuario {

  static final PreferenciasUsuario _instancia = new PreferenciasUsuario._internal();

  factory PreferenciasUsuario(){
    return _instancia;
  }

  PreferenciasUsuario._internal();

  SharedPreferences _prefs;

  intiPrefs() async{
    this._prefs = await SharedPreferences.getInstance();
  }


  //GET Y SET DEL color secundario
  get colorSecundario {
    return _prefs.getInt('colorSecundario') ?? 1;
  }

  set colorSecundario (int value) {
    _prefs.setInt('colorSecundario', value);
  }
}