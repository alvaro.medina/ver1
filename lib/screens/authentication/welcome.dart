import 'dart:ui';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:restaurant_app/screens/authentication/login.dart';
import 'package:restaurant_app/services/auth_services.dart';
import 'package:restaurant_app/widgets/loading.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({Key key}) : super(key: key);

  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  static const AssetImage mainImage = AssetImage('assets/welcome.jpg');
  static const AssetImage logoApp = AssetImage('assets/sirvepeLogo.png');

  //String error = '';
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: mainImage,
                  colorFilter: ColorFilter.mode(
                    //Darker gradient
                    Colors.black.withOpacity(0.5),
                    BlendMode.srcOver,
                  ),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 150),
                    //color: Colors.red,
                    height: 100.0,
                    child: Image(
                      image: logoApp,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Container(
                    //color: Colors.blue,
                    padding:
                        EdgeInsets.symmetric(horizontal: 60.0, vertical: 20.0),
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                          style: Theme.of(context).textTheme.headline2.copyWith(
                                fontSize: 16.0,
                              ),
                          text: 'Nunca fue tan facil pedir delivery.'),
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
                bottom: 100,
                left: 50,
                right: 50,
                child: Column(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: RaisedButton(
                        onPressed: () {
                          print('Ingrese button');
                          //Navigator.pushNamed(context, '/login');
                          Navigator.of(context).push(new MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  new LoginPage()));
                        },
                        //color: Colors.transparent,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0),
                          side: BorderSide(
                            color: Colors.white.withOpacity(0.4),
                          ),
                        ),
                        child: loading
                            ? Loading(
                                color: Theme.of(context).accentColor,
                              )
                            : Text(
                                'Ingrese',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline2
                                    .copyWith(fontSize: 14),
                              ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.only(top: 5.0),
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        onPressed: () async {
                          setState(() => loading = true);
                          UserCredential response = await context
                              .read<AuthenticationService>()
                              .signInWithGoogle();
                          if (response == null) {
                            setState(() => loading = false);
                          }
                        },
                        color: Colors.white,
                        child: loading
                            ? Loading()
                            : Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image(
                                    image: AssetImage('assets/google.png'),
                                    width: 20.0,
                                    height: 20.0,
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 10.0),
                                    child: Text(
                                      'Ingrese con Google',
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 15.0),
                                    ),
                                  ),
                                ],
                              ),
                      ),
                    ),
                  ],
                ))
          ],
        ),
      ),
    );
  }
}
