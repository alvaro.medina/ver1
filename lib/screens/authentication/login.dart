import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:restaurant_app/services/auth_services.dart';
import 'package:restaurant_app/widgets/back_button.dart';
import 'package:restaurant_app/widgets/loading.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  static const AssetImage loginImage = AssetImage('assets/login.jpg');
  //Variables de formulario
  final _formKey = GlobalKey<FormState>();
  String error = '';
  bool loading = false;
  // text field state
  String email = '';
  String password = '';

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    var screenwidth = MediaQuery.of(context).size.width;

    Widget _emailInput() {
      return Container(
        margin: EdgeInsets.only(top: 40.0),
        padding: EdgeInsets.only(left: 20.0),
        decoration: BoxDecoration(
            color: Color.fromRGBO(142, 142, 147, 1.2),
            borderRadius: BorderRadius.circular(30.0)),
        child: TextFormField(
          initialValue: email,
          validator: (val) {
            Pattern pattern =
                r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
            RegExp regex = new RegExp(pattern);

            if (val.isEmpty) return "Es necesario especificar un email";
            if (!regex.hasMatch(val))
              return "Ingrese una dirección de correo válida";
            return null;
          },
          onChanged: (val) {
            setState(() => email = val);
          },
          keyboardType: TextInputType.emailAddress,
          decoration: InputDecoration(
            hintText: 'Email',
            border: OutlineInputBorder(borderSide: BorderSide.none),
          ),
        ),
      );
    }

    Widget _passwordInput() {
      return Container(
        margin: EdgeInsets.only(top: 15.0),
        padding: EdgeInsets.only(left: 20.0),
        decoration: BoxDecoration(
            color: Color.fromRGBO(142, 142, 147, 1.2),
            borderRadius: BorderRadius.circular(30.0)),
        child: TextFormField(
          validator: (val) => val.length < 6 ? 'Ingrese +6 caracteres.' : null,
          onChanged: (val) {
            setState(() => password = val);
          },
          obscureText: true,
          decoration: InputDecoration(
            hintText: 'Contraseña',
            border: OutlineInputBorder(borderSide: BorderSide.none),
          ),
        ),
      );
    }

    Widget _buttonLogin(BuildContext context) {
      return Container(
        width: 420.0,
        height: 50.0,
        margin: EdgeInsets.only(top: 30.0),
        child: RaisedButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
            onPressed: () async {
              if (_formKey.currentState.validate()) {
                setState(() => loading = true);
                UserCredential response = await context
                    .read<AuthenticationService>()
                    .signIn(email: email.trim(), password: password.trim());
                if (response == null) {
                  setState(() =>
                      {loading = false, error = "Credenciales incorrectas!"});
                } else {
                  Navigator.pop(context);
                }
              }
            },
            //color: Theme.of(context).accentColor,
            child: Text('Login',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold))),
      );
    }

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              children: [
                Container(
                  height: screenHeight / 2.8,
                  width: screenwidth,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: loginImage,
                      colorFilter: ColorFilter.mode(
                        //Darker gradient
                        Colors.black.withOpacity(0.6),
                        BlendMode.srcOver,
                      ),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 50.0),
                  child: backButton(context, Colors.white),
                ),
              ],
            ),
            Transform.translate(
              offset: Offset(0.0, -20.0),
              child: Container(
                  width: screenwidth,
                  height: screenHeight / 1.4,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20.0)),
                  child: Form(
                    key: _formKey,
                    child: Padding(
                      padding: EdgeInsets.all(30.0),
                      child: Center(
                        child: loading
                            ? Loading()
                            : SingleChildScrollView(
                                child: Column(
                                  children: [
                                    Text('BIENVENIDO',
                                        style: TextStyle(
                                            color:
                                                Theme.of(context).primaryColor,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 30.0)),
                                    Text('Ingresa a tu cuenta',
                                        style: TextStyle(
                                            color: Colors.grey,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 15.0)),
                                    _emailInput(),
                                    _passwordInput(),
                                    _buttonLogin(context),
                                    SizedBox(height: 12.0),
                                    Text(
                                      error,
                                      style: TextStyle(
                                          color: Colors.red, fontSize: 14.0),
                                    ),
                                  ],
                                ),
                              ),
                      ),
                    ),
                  )),
            ),
          ],
        ),
      ),
    );
  }
}

/*
  @override
  Widget build(BuildContext context) {
    return loading
        ? Loading()
        : Scaffold(
            backgroundColor: Colors.brown[100],
            appBar: AppBar(
              backgroundColor: Colors.brown[400],
              elevation: 0.0,
              title: Text('Sign in to Brew Crew'),
              actions: <Widget>[
                FlatButton.icon(
                  icon: Icon(Icons.person),
                  label: Text('Register'),
                  onPressed: () => {print("Cambiar a register")},
                ),
              ],
            ),
            body: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 20.0),
                      TextFormField(
                        initialValue: email,
                        decoration:
                            textInputDecoration.copyWith(hintText: 'Correo'),
                        validator: (val) {
                          Pattern pattern =
                              r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                          RegExp regex = new RegExp(pattern);

                          if (val.isEmpty)
                            return "Es necesario especificar un email";

                          if (!regex.hasMatch(val)) return 'Enter Valid Email';

                          return null;
                        },
                        onChanged: (val) {
                          setState(() => email = val);
                        },
                        keyboardType: TextInputType.emailAddress,
                      ),
                      SizedBox(height: 20.0),
                      TextFormField(
                        obscureText: true,
                        decoration: textInputDecoration.copyWith(
                            hintText: 'Contraseña'),
                        validator: (val) => val.length < 6
                            ? 'Enter a password 6+ chars long'
                            : null,
                        onChanged: (val) {
                          setState(() => password = val);
                        },
                        keyboardType: TextInputType.text,
                      ),
                      SizedBox(height: 20.0),
                      RaisedButton(
                          color: Colors.pink[400],
                          child: Text(
                            'Sign In',
                            style: TextStyle(color: Colors.white),
                          ),
                          onPressed: () async {
                            if (_formKey.currentState.validate()) {
                              setState(() => loading = true);
                              UserCredential response = await context
                                  .read<AuthenticationService>()
                                  .signIn(
                                      email: email.trim(),
                                      password: password.trim());
                              if (response == null) {
                                setState(() => {
                                      loading = false,
                                      error = "Credenciales incorrectas!"
                                    });
                              }
                            }
                          }),
                      SizedBox(height: 12.0),
                      Text(
                        error,
                        style: TextStyle(color: Colors.red, fontSize: 14.0),
                      ),
                      FlatButton(
                        onPressed: () async {
                          setState(() => loading = true);
                          UserCredential response = await context
                              .read<AuthenticationService>()
                              .signInWithGoogle();
                          if (response == null) {
                            setState(() => loading = false);
                          }
                        },
                        color: Colors.pink[400],
                        child: Text(
                          'Ingresar con google',
                          style: TextStyle(color: Colors.white),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ));
  }
}
*/
