import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:restaurant_app/models/restaurant.dart';
import 'package:restaurant_app/screens/home/restaurant/detail_page.dart';
import 'package:restaurant_app/screens/home/secondary.dart';
import 'package:restaurant_app/services/auth_services.dart';
import 'package:restaurant_app/services/nav_services.dart';
import 'package:restaurant_app/widgets/appBar.dart';
import 'package:restaurant_app/widgets/cardHome.dart';

class DashboardPage extends StatefulWidget {
  static const route = '/home';
  //final AnimationController animationController;
  //const DashboardPage({Key key, this.animationController}) : super(key: key);

  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage>
    with TickerProviderStateMixin {
  static const AssetImage homeImage = AssetImage('assets/home.png');
  @override
  Widget build(BuildContext context) {
    var restaurants = Provider.of<List<Restaurant>>(context);
    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: appBar(context),
      extendBodyBehindAppBar: true,
      body: SingleChildScrollView(
        child: Transform.translate(
          offset: Offset(0.0, 0.0),
          child: Stack(
            children: [
              Container(
                height: 790,
                width: screenWidth,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: homeImage,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(
                height: screenHeight,
                decoration: BoxDecoration(
                    //color: Colors.white,
                    gradient: LinearGradient(
                        begin: FractionalOffset.topLeft,
                        end: FractionalOffset.topRight,
                        colors: [
                      Color(0xffec7633).withOpacity(0.9),
                      Color(0xfff1ad3d).withOpacity(0.8),
                    ],
                        stops: [
                      0.0,
                      1.0
                    ])),
              ),
              Positioned(
                top: screenHeight / 3,
                right: 0,
                left: 0,
                height: screenHeight,
                child: Container(
                  //color: Colors.white,
                  color: Color(0xffe9e9ea),
                  //color:Colors.red,
                ),
              ),
              Positioned(
                top: 50,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    /*
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.baseline,
                      children: [
                        Text(
                          'Restaurantes populares',
                          style: Theme.of(context).textTheme.headline2.copyWith(
                                fontSize: 16,
                                fontWeight: FontWeight.w700,
                              ),
                        ),
                        Text(
                          'Ver todos',
                          style: Theme.of(context)
                              .textTheme
                              .headline2
                              .copyWith(fontSize: 13),
                        ),
                      ],
                    ),
*/
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            margin:
                                EdgeInsets.only(top: 25, left: 20, right: 20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.baseline,
                              children: [
                                Text(
                                  'Restaurantes populares',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline2
                                      .copyWith(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w700,
                                      ),
                                ),
                                Text(
                                  'Ver todos',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline2
                                      .copyWith(fontSize: 13),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            height: 300,
                            width: screenWidth,
                            //color: Colors.red,
                            child: restaurants != null
                                ? ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    shrinkWrap: true,
                                    itemCount: restaurants.length,
                                    itemBuilder: (context, index) {
                                      Restaurant restaurant =
                                          restaurants[index];
                                      return CustomCard(restaurant: restaurant);
                                    },
                                  )
                                : Text(
                                    'nothing to show'), //REFERENCIA DE CARD CUSTOM
                          ),
                        ],
                      ),
                    ),
                    CustomFoodCategories(),
                    CustomTrendingFoods(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CustomCard extends StatelessWidget {
  final Restaurant restaurant;

  const CustomCard({
    Key key,
    this.restaurant,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var photoURL = restaurant.photoURL;
    var nombre = restaurant.nombre;
    var rating = restaurant.rating;
    var categoria = restaurant.categoria;
    var ubicacion = restaurant.ubicacion;

    return GestureDetector(
      onTap: () {
        print('$nombre');
        Navigator.of(context).push(
          new MaterialPageRoute(
            builder: (BuildContext context) => new RestaurantDetailPage(restaurant: restaurant,),
          ),
        );
      },
      child: Container(
        margin: EdgeInsets.all(20),
        width: MediaQuery.of(context).size.width / 1.3,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Stack(
              children: [
                Container(
                  height: 200,
                  decoration: BoxDecoration(
                    //color: Colors.orange[50],
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10),
                    ),
                    image: DecorationImage(
                      image: NetworkImage(photoURL),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        padding: EdgeInsets.all(6),
                        child: Text(
                          'ABIERTO',
                          style: Theme.of(context).textTheme.headline2.copyWith(
                                fontSize: 9,
                                color: Colors.green,
                              ),
                        ),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(5),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            top: 6, bottom: 6, left: 8, right: 8),
                        child: Row(
                          children: [
                            Icon(
                              Icons.star,
                              size: 10.0,
                              color: Colors.yellow[300],
                            ),
                            SizedBox(
                              width: 4,
                            ),
                            Text(
                              rating,
                              style: Theme.of(context)
                                  .textTheme
                                  .headline1
                                  .copyWith(fontSize: 9),
                            ),
                          ],
                        ),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10),
                ),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    //spreadRadius: 2,
                    blurRadius: 6,
                  ),
                ],
              ),
              padding: EdgeInsets.only(left: 10),
              height: 60,
              width: MediaQuery.of(context).size.width,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        nombre,
                        style: Theme.of(context)
                            .textTheme
                            .headline1
                            .copyWith(fontSize: 15),
                      ),
                      SizedBox(
                        width: 6,
                      ),
                      Container(
                        padding: EdgeInsets.all(2),
                        child: Text(
                          categoria,
                          style: Theme.of(context)
                              .textTheme
                              .headline2
                              .copyWith(fontSize: 9),
                        ),
                        decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      SizedBox(
                        width: 6,
                      ),
                      Container(
                        padding: EdgeInsets.all(2),
                        child: Text(
                          'Bar',
                          style: Theme.of(context)
                              .textTheme
                              .headline2
                              .copyWith(fontSize: 9),
                        ),
                        decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 3),
                  Text(
                    ubicacion,
                    style: Theme.of(context)
                        .textTheme
                        .bodyText2
                        .copyWith(fontSize: 12),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class CustomFoodCategories extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 140,
      //color: Colors.red,//MARGENES DE VISTA
      width: MediaQuery.of(context).size.width,
      child: Container(
        margin: EdgeInsets.only(left: 20, top: 10, bottom: 10),
        //color: Colors.yellow, //MARGENES DE VISTA
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.baseline,
              children: [
                Text(
                  'Categorias',
                  style: Theme.of(context).textTheme.headline1.copyWith(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                      ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 20),
                  child: Text(
                    'Ver todos(5)',
                    style: Theme.of(context)
                        .textTheme
                        .headline1
                        .copyWith(fontSize: 13),
                  ),
                ),
              ],
            ),
            //AQUI DEBE IR UN LISTVIEW PARA LISTAR SEGUN LA CATEGORIA
            SizedBox(height: 5),

            Row(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 5, bottom: 5, right: 5),
                  height: 80,
                  width: 80,
                  child: Stack(
                    children: [
                      Container(
                        //color: Colors.blue,
                        decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(5),
                          image: DecorationImage(
                            image: NetworkImage(
                                "https://www.cursosgastronomia.com.mx/wp-content/uploads/2014/03/Nomad-in-New-York-768x522.jpg"),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            gradient: LinearGradient(
                                begin: FractionalOffset.topLeft,
                                end: FractionalOffset.topRight,
                                colors: [
                                  Color(0xffec7633).withOpacity(0.6),
                                  Color(0xfff1ad3d).withOpacity(0.4),
                                ],
                                stops: [
                                  0.0,
                                  1.0
                                ])),
                      ),
                      Container(
                        child: Center(
                          child: Text(
                            'Pizza',
                            style: Theme.of(context)
                                .textTheme
                                .headline2
                                .copyWith(fontSize: 13),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 5, bottom: 5, right: 5),
                  height: 80,
                  width: 80,
                  child: Stack(
                    children: [
                      Container(
                        //color: Colors.blue,
                        decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(5),
                          image: DecorationImage(
                            image: NetworkImage(
                                "https://www.cursosgastronomia.com.mx/wp-content/uploads/2014/03/Nomad-in-New-York-768x522.jpg"),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            gradient: LinearGradient(
                                begin: FractionalOffset.topCenter,
                                end: FractionalOffset.bottomCenter,
                                colors: [
                                  Theme.of(context)
                                      .accentColor
                                      .withOpacity(0.6),
                                  Colors.purple.withOpacity(0.6),
                                ],
                                stops: [
                                  0.0,
                                  1.0
                                ])),
                      ),
                      Container(
                        child: Center(
                          child: Text(
                            'China',
                            style: Theme.of(context)
                                .textTheme
                                .headline2
                                .copyWith(fontSize: 13),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class CustomTrendingFoods extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height / 3.3,
      //color: Colors.red, //MARGENES DE VISTA
      width: MediaQuery.of(context).size.width,
      child: Container(
        margin: EdgeInsets.only(left: 20, top: 10, bottom: 10),
        //color: Colors.yellow, //MARGENES DE VISTA
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.baseline,
              children: [
                Text(
                  'Platos populares',
                  style: Theme.of(context).textTheme.headline1.copyWith(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                      ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 20),
                  child: Text(
                    'Ver todos(10)',
                    style: Theme.of(context)
                        .textTheme
                        .headline1
                        .copyWith(fontSize: 13),
                  ),
                ),
              ],
            ),
            SizedBox(height: 5),
            Row(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 5, bottom: 5, right: 5),
                  //color: Colors.red, <---
                  height: 160,
                  width: MediaQuery.of(context).size.width / 2.2,
                  child: Stack(
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height,
                        //color: Colors.green[100], //DIVISION DE STACK <---
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          top: 10,
                          left: 10,
                          bottom: 10,
                          right: 25,
                        ),
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Container(
                          margin: EdgeInsets.all(6),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Copa de Leche de trigre a la carta',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline1
                                    .copyWith(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w800),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                'S/. 25.00',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline5
                                    .copyWith(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w700),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Positioned(
                        bottom: 0,
                        right: 0,
                        child: Container(
                          width: 100,
                          height: 100,
                          decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black.withOpacity(0.7),
                                //spreadRadius: 2,
                                blurRadius: 6,
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 5, bottom: 5, right: 5),
                  //color: Colors.red, <---
                  height: 160,
                  width: MediaQuery.of(context).size.width / 2.2,
                  child: Stack(
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height,
                        //color: Colors.green[100], //DIVISION DE STACK <---
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          top: 10,
                          left: 10,
                          bottom: 10,
                          right: 25,
                        ),
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Container(
                          margin: EdgeInsets.all(6),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Copa de Leche de trigre a la carta',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline1
                                    .copyWith(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w800),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                'S/. 25.00',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline5
                                    .copyWith(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w700),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Positioned(
                        bottom: 0,
                        right: 0,
                        child: Container(
                          width: 100,
                          height: 100,
                          decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black.withOpacity(0.7),
                                //spreadRadius: 2,
                                blurRadius: 6,
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
