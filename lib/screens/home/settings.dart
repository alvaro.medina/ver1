import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:restaurant_app/share_prefs/preferencias_usuario.dart';
import 'package:restaurant_app/theme/theme.dart';


class SettingsPage extends StatelessWidget {

  final AnimationController animationController;

  const SettingsPage({Key key, this.animationController}) : super(key: key);

  @override
  Widget build(BuildContext context) {


    //theme
    final appTheme = Provider.of<ThemeChanger>(context);

    //ultimo theme
    final prefs = new PreferenciasUsuario();

    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        children: [
            SafeArea(
              child: Container(
                width: double.infinity,
                height: 200,
                child: CircleAvatar(
                  backgroundColor:  appTheme.currentTheme.accentColor,
                  child: Text('ON', style: TextStyle(fontSize:50),),
                ),
              ),
            ),
            Expanded(
              child: _ListaOpciones(),
            ),
            SafeArea(
              child: ListTile(
                leading: Icon(Icons.lightbulb_outline, color:  appTheme.currentTheme.accentColor),
                title: Text('Dark Mode'),
                trailing: Switch.adaptive(
                  value: appTheme.darkTheme, 
                  activeColor:  appTheme.currentTheme.accentColor,
                  onChanged: (value){
                     appTheme.darkTheme = value;
                     if(value == true){
                      prefs.colorSecundario = 2;
                     }else{
                      prefs.colorSecundario = 1; 
                     }
                  }
                ),
              ),
            ),
            SafeArea(
              bottom: true,
              top: false,
              left: false,
              right: false,
              child: ListTile(
                leading: Icon(Icons.add_to_home_screen, color:  appTheme.currentTheme.accentColor),
                title: Text('Custom Theme'),
                trailing: Switch.adaptive(
                  value: appTheme.customTheme, 
                  activeColor:  appTheme.currentTheme.accentColor,
                  onChanged: (value){
                     appTheme.customTheme = value;
                     if(value == true){
                      prefs.colorSecundario = 3;
                     }else{
                      prefs.colorSecundario = 1; 
                     }
                  }
                ),
              ),
            ),
        ],
      ),
    );
  }
}

class _ListaOpciones extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final appTheme = Provider.of<ThemeChanger>(context);
    final items = ['Profile', 'Settings', 'About us...', 'Sign Out'];
    final icons =[Icons.supervised_user_circle, Icons.settings, Icons.error, Icons.exit_to_app_rounded];


    return ListView.separated(
      physics: BouncingScrollPhysics(),
      separatorBuilder:(context, i) => Divider(
        color: Colors.orange,
      ),
      itemCount: items.length,
      itemBuilder: (context, index) => ListTile(

        leading: Icon(icons[index], color:  appTheme.currentTheme.accentColor),
        title: Text(items[index]),
        trailing: Icon(Icons.chevron_right, color: appTheme.currentTheme.accentColor),
        onTap: () {

        },

      ),

    );
  }
}
