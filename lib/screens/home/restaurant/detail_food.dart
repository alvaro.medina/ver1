import 'package:flutter/material.dart';
import 'package:restaurant_app/models/food.dart';
import 'package:restaurant_app/screens/home/payment/food_checklist.dart';
import 'package:restaurant_app/widgets/back_button.dart';

class DetailFood extends StatelessWidget {
  static const route = '/detailFood';
  static const AssetImage homeImage = AssetImage('assets/home.png');

  final food = Food("Salchipapita", 30, 2,
      description: "Salchipapa norteña",
      pictureUrl:
          "https://t1.rg.ltmcdn.com/es/images/8/3/5/salchipapas_74538_600_square.jpg");
  //const DetailFood({Key key, this.food}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
    var screenHeight = MediaQuery.of(context).size.height;

    /*
    var name = food.name;
    var description = food.description;
    var price = food.price;
    var quantity = food.quantity;
    var picture_url = food.pictureUrl;*/

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            /*Stack(
              children: [
                Container(
                  height: 60,
                  decoration: BoxDecoration(
                    //color: Colors.orange[100],
                    gradient: LinearGradient(
                      begin: FractionalOffset.topLeft,
                      end: FractionalOffset.topRight,
                      colors: [
                        Color(0xffec7633).withOpacity(0.9),
                        Color(0xfff1ad3d).withOpacity(0.9),
                      ],
                      stops: [0.0, 1.0],
                    ),
                  ),
                ),
                Positioned(
                  top: 14,
                  left: 10,
                  child: backButton(context, Colors.white),
                ),
              ],
            ),
            Stack(
              children: [
                Container(
                  height: 160,
                  width: screenWidth,
                  //color: Colors.red[100],
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: NetworkImage(
                          "https://www.peru-retail.com/wp-content/uploads/DSC02503.jpg"),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Container(
                  height: 160,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: FractionalOffset.topLeft,
                      end: FractionalOffset.topRight,
                      colors: [
                        Color(0xffec7633).withOpacity(0.3),
                        Color(0xfff1ad3d).withOpacity(0.2),
                      ],
                      stops: [0.0, 1.0],
                    ),
                  ),
                ),
              ],
            ),
            */
            Stack(
              children: [
                Container(
                  height: 150,
                  width: screenWidth,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.elliptical(screenWidth / 2.5, 85),
                    ),
                    image: DecorationImage(
                      image: homeImage,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Container(
                  height: 150,
                  decoration: BoxDecoration(
                    //color: Colors.orange[100],
                    gradient: LinearGradient(
                      begin: FractionalOffset.topLeft,
                      end: FractionalOffset.topRight,
                      colors: [
                        Color(0xffec7633).withOpacity(0.9),
                        Color(0xfff1ad3d).withOpacity(0.8),
                      ],
                      stops: [0.0, 1.0],
                    ),
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.elliptical(screenWidth / 2.5, 85),
                    ),
                  ),
                ),
                Positioned(
                  right: 20,
                  bottom: 20,
                  child: Text(
                    'Salchipapa Norteña',
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ),
                Positioned(
                  top: 14,
                  left: 10,
                  child: backButton(context, Colors.white),
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 25),
              height: screenHeight / 3.2,
              width: screenWidth / 1.3,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                image: DecorationImage(
                  image: homeImage,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(right: 20, left: 20, bottom: 15),
              //height: 200,
              width: screenWidth,
              //color: Colors.red[100],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: screenWidth / 1.5,
                    //color: Colors.orange[100],
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Salchipapa norteña',
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                        SizedBox(height: 4),
                        Text(
                          'Av Independencia 410',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText2
                              .copyWith(fontSize: 12),
                        ),
                        SizedBox(height: 3),
                        Row(
                          children: [
                            Text(
                              '4.5',
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText2
                                  .copyWith(fontSize: 12),
                            ),
                            SizedBox(width: 3),
                            Icon(
                              Icons.star,
                              size: 14,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Center(
                      child: Container(
                        //color: Colors.purple[100],
                        child: Text(
                          'S/. 12.00', //---------------------EDIT TEXT
                          style: Theme.of(context)
                              .textTheme
                              .headline5
                              .copyWith(fontWeight: FontWeight.w700),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(right: 20, left: 20, bottom: 20),
              //height: 200,
              width: screenWidth,
              //color: Colors.purple[100],
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Descripcion',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  SizedBox(height: 4),
                  Text(
                    'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText2
                        .copyWith(fontSize: 12, height: 1.5),
                  ),
                  SizedBox(height: 3),
                ],
              ),
            ),
            //PARTE DE AGREGAR AL CARRITO
            Container(
              //padding: EdgeInsets.all(10),
              height: screenHeight / 12,
              //color: Colors.yellow[200],
              child: Row(
                children: <Widget>[
                  Container(
                    width: screenWidth / 2.1,
                    //color: Colors.blue[200],
                    child: Container(
                      decoration: BoxDecoration(
                        color: Color(0xffe9e9ea),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.2),
                            spreadRadius: 2,
                            blurRadius: 6,
                            offset: Offset(0, 3),
                          ),
                        ],
                      ),
                      child: Center(
                        child: Text(
                          'Agregar al carrito',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1
                              .copyWith(fontSize: 16),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(
                          new MaterialPageRoute(
                            builder: (BuildContext context) =>
                                new Payment_Checklist(
                              food: food,
                              //---INFO A PASAR
                            ),
                          ),
                        );
                      },
                      child: Stack(
                        children: [
                          Container(
                            height: screenHeight / 12,
                            decoration: BoxDecoration(
                              //color: Colors.white,
                              gradient: LinearGradient(
                                begin: FractionalOffset.topLeft,
                                end: FractionalOffset.topRight,
                                colors: [
                                  Color(0xffec7633).withOpacity(1),
                                  Color(0xfff1ad3d).withOpacity(1),
                                ],
                                stops: [0.0, 1.0],
                              ),
                            ),
                          ),
                          Container(
                            height: screenHeight / 12,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Comprar ahora',
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline2
                                        .copyWith(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 16)),
                                Theme(
                                  //OVERRIDE de color de Icono
                                  data: Theme.of(context).copyWith(
                                    iconTheme: Theme.of(context)
                                        .iconTheme
                                        .copyWith(color: Colors.white),
                                  ),
                                  child: Icon(Icons.chevron_right),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
