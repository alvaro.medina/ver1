import 'package:flutter/material.dart';
import 'package:restaurant_app/models/restaurant.dart';
import 'package:restaurant_app/screens/home/restaurant/detail_food.dart';
import 'package:restaurant_app/widgets/back_button.dart';

class RestaurantDetailPage extends StatelessWidget {
  static const route = '/detail';

  final Restaurant restaurant;
  const RestaurantDetailPage({Key key, this.restaurant}) : super(key: key);
  static const AssetImage homeImage = AssetImage('assets/home.png');

  @override
  Widget build(BuildContext context) {
    var name = restaurant.nombre;

    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              alignment: Alignment.topLeft,
              children: [
                Container(
                  height: screenHeight / 3.5,
                  width: screenWidth,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: homeImage,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Container(
                  height: screenHeight / 3.5,
                  decoration: BoxDecoration(
                    //color: Colors.white,
                    gradient: LinearGradient(
                      begin: FractionalOffset.topLeft,
                      end: FractionalOffset.topRight,
                      colors: [
                        Color(0xffec7633).withOpacity(0.6),
                        Color(0xfff1ad3d).withOpacity(0.6),
                      ],
                      stops: [0.0, 1.0],
                    ),
                  ),
                ),
                Positioned(
                  bottom: 10,
                  right: 10,
                  child: GestureDetector(
                    onTap: () {
                      print('Ubicacion seleccionada');
                    },
                    child: Container(
                      padding: EdgeInsets.only(
                          right: 9, left: 9, top: 10, bottom: 10),
                      decoration: BoxDecoration(
                        color: Color(0xfff4f5f7),
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'Mostrar ubicacion',
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1
                                .copyWith(
                                    fontSize: 12, fontWeight: FontWeight.w700),
                          ),
                          SizedBox(
                            width: 3,
                          ),
                          Icon(
                            Icons.gps_fixed,
                            size: 12,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: 14,
                  left: 10,
                  child: backButton(context, Colors.white),
                ),
              ],
            ),
            Container(
              height: screenHeight,
              //color: Color(0xffe9e9ea),
              padding: EdgeInsets.all(20),
              //color: Colors.red[50],
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        restaurant.nombre, //-------------------------TEXT EDIT
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                      SizedBox(
                        width: 9,
                      ),
                      Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 6, vertical: 2),
                        child: Text(
                          'Bar', //--------------------------------------TEXT EDIT
                          style: Theme.of(context)
                              .textTheme
                              .headline2
                              .copyWith(fontSize: 9),
                        ),
                        decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 3),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        restaurant.ubicacion, //-----------------TEXT EDIT
                        style: Theme.of(context)
                            .textTheme
                            .bodyText2
                            .copyWith(fontSize: 13),
                      ),
                      Row(
                        children: [
                          Text(
                            restaurant.rating, //---------------------------TEXT EDIT
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1
                                .copyWith(fontSize: 13),
                          ),
                          Icon(
                            Icons.star,
                            size: 13,
                          )
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: 13),
                  Text(
                    'Descripcion',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  SizedBox(height: 5),
                  Text(
                    restaurant.descripcion,
                    style: Theme.of(context).textTheme.bodyText2,
                    textAlign: TextAlign.justify,
                  ),
                  SizedBox(height: 15),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.baseline,
                    children: [
                      Text(
                        'Lista de menu',
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                      Text(
                        'Ver todos',
                        style: Theme.of(context)
                            .textTheme
                            .bodyText2
                            .copyWith(fontSize: 12),
                      ),
                    ],
                  ),
                  Container(
                    //height: 250,
                    width: screenWidth,
                    //color: Colors.blue[50],
                    child: MenuListWidget(),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MenuListWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(
          new MaterialPageRoute(
            builder: (BuildContext context) => new DetailFood(
                //---INFO A PASAR
                ),
          ),
        );
      },
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 170,
              width: MediaQuery.of(context).size.width / 2.2,
              //color: Colors.purple[100],
              margin: EdgeInsets.only(top: 15, right: 10),
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      //color: Colors.orange[50],
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10),
                      ),
                      image: DecorationImage(
                        image: NetworkImage(
                            "https://www.peru-retail.com/wp-content/uploads/DSC02503.jpg"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Positioned(
                    top: 10,
                    left: 10,
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Text(
                        'S/. 30.00',
                        style: Theme.of(context).textTheme.headline5.copyWith(
                            fontSize: 12, fontWeight: FontWeight.w700),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              //height: 50,
              width: MediaQuery.of(context).size.width / 2.2,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10),
                ),
              ),
              child: Container(
                
                padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                decoration: BoxDecoration(
                  color: Color(0xffe9e9ea),
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(10),
                    bottomLeft: Radius.circular(10),
                  ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Salchipapa',
                      style: Theme.of(context).textTheme.bodyText1.copyWith(
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                          ),
                    ),
                    SizedBox(height: 4),
                    Row(
                      children: [
                        Text(
                          '3.5',
                          style: Theme.of(context).textTheme.bodyText2.copyWith(
                                fontSize: 12,
                                fontWeight: FontWeight.w500,
                              ),
                        ),
                        SizedBox(width: 3),
                        Icon(
                          Icons.star,
                          size: 12,
                        ),
                        Icon(
                          Icons.star,
                          size: 12,
                        ),
                        Icon(
                          Icons.star,
                          size: 12,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
