import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:restaurant_app/models/food.dart';
import 'package:mercado_pago_mobile_checkout/mercado_pago_mobile_checkout.dart';
//https://pub.dev/packages/mercado_pago_mobile_checkout   Module used

// ACESS TOKEN:  TEST-4645320912006116-121903-9bcc30752832a2d0815f9b829d50775f-455920374
//Public Key :  TEST-0232429f-a666-42d1-a45a-5843406cd30e

//https://www.mercadopago.com.pe/developers/es/guides/online-payments/mobile-checkout/testing

class Payment_Checklist extends StatefulWidget {
  static const route = '/checklist';
  final Food food;
  const Payment_Checklist({Key key, this.food}) : super(key: key);
  // invoking widget.food.PARAMETER brings values in stateful

  @override
  _Payment_ChecklistState createState() => _Payment_ChecklistState();
}

class _Payment_ChecklistState extends State<Payment_Checklist> {
  @override
  void initState() {
    super.initState();
    createOrder();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      
      child: Container(
        child: Center(
          child: Text(widget.food.name),
        ),
      ),
    );
  }

  void createOrder() async {
    //var orderRef = Firestore.instance.collection('collectionPath').document();
    //await orderRef.set(widget.food.toMap());
    var result = await MercadoPagoMobileCheckout.startCheckout(
      "TEST-0232429f-a666-42d1-a45a-5843406cd30e",
      "455920374-462aad0a-70ec-4032-aa06-cbdad4ca9bed");

      print(result);
      print("---------kk-");
      print(result);

      print(result.toJson());

      var resultado = result.result.toString();
      print("Este es el resultado:  "+resultado);
  }
}




/*
class Payment_Checklist extends StatelessWidget {
  static const route = '/checklist';
  final Food food;
  const Payment_Checklist({Key key, this.food}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        child: Center(
          child: Text(food.name),
        ),
      ),
    );
  }
}
*/
