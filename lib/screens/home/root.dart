import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:restaurant_app/services/auth_services.dart';
//import 'package:restaurant_app/screens/home/dashboard.dart';
//import 'package:restaurant_app/screens/home/principal.dart';
//import 'package:restaurant_app/screens/home/profile.dart';
//import 'package:restaurant_app/services/auth_services.dart';
import 'package:restaurant_app/services/nav_services.dart';
import 'package:restaurant_app/widgets/bottom_bar.dart';
//import 'package:restaurant_app/widgets/bottom_bar.dart';

class RootPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<NavigationProvider>(
      builder: (context, provider, child) {
        // Initialize [Navigator] instance for each screen.
        final screens = provider.screens
            .map(
              (screen) => Navigator(
                key: screen.navigatorState,
                onGenerateRoute: screen.onGenerateRoute,
              ),
            )
            .toList();

        return WillPopScope(
          onWillPop: () async => provider.onWillPop(context),
          child: Scaffold(
              body: SafeArea(
                child: IndexedStack(
                  children: screens,
                  index: provider.currentTabIndex,
                ),
              ),
              floatingActionButton: FloatingActionButton(
                onPressed: () {
                  print("New plato");
                },
                backgroundColor: Theme.of(context).primaryColor,
                
                child: Icon(
                  Icons.fastfood,
                  color: Colors.white,
                ),
              ),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.centerDocked,
              bottomNavigationBar: BottomBar(
                index: provider.currentTabIndex,
                setTab: provider.setTab,
              )),
        );
      },
    );
  }
}

/*
class RootPage extends StatefulWidget {
  RootPage({Key key}) : super(key: key);

  @override
  _RootPageState createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> with TickerProviderStateMixin {
  AnimationController animationController;
  Widget tabBody = Container(
    color: Colors.white,
  );
  String currentTab;

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 600), vsync: this);
    tabBody = DashboardPage(animationController: animationController);
    currentTab = "/";
    super.initState();
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange,
        elevation: 3.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(20),
          ),
        ),
        centerTitle: true,
        title: Image.asset(
          'assets/sirVePE.png',
          fit: BoxFit.contain,
          height: 48.0,
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.exit_to_app, color: Colors.white),
            onPressed: () {
              context.read<AuthenticationService>().signOut();
            },
          ),
        ],
      ),
      body: FutureBuilder<bool>(
        future: getData(),
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          if (!snapshot.hasData) {
            return const SizedBox();
          } else {
            return WillPopScope(
              child: tabBody,
              onWillPop: () async {
                if (currentTab != "/") {
                  animationController.reverse().then<dynamic>((data) {
                    if (!mounted) {
                      return;
                    }
                    setState(() {
                      tabBody = DashboardPage(
                          animationController: animationController);
                      currentTab = "/";
                    });
                  });
                  return false;
                }
                return true;
              },
            );
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          print("New plato");
        },
        backgroundColor: Color(0xFFF17532),
        child: Icon(
          Icons.fastfood,
          color: Colors.white,
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomBar(
        index: currentTab,
        changeIndex: (String route) {
          if (route == "/") {
            animationController.reverse().then<dynamic>((data) {
              if (!mounted) {
                return;
              }
              setState(() {
                tabBody =
                    DashboardPage(animationController: animationController);
                currentTab = "/";
              });
            });
          } else if (route == "/profile") {
            animationController.reverse().then<dynamic>((data) {
              if (!mounted) {
                return;
              }
              setState(() {
                tabBody = ProfilePage(animationController: animationController);
                currentTab = "/profile";
              });
            });
          } else if (route == "/principal") {
            animationController.reverse().then<dynamic>((data) {
              if (!mounted) {
                return;
              }
              setState(() {
                tabBody = PrincipalPage();
                currentTab = "/principal";
              });
            });
          }
        },
      ),
    );
  }
}
*/
