import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:mapbox_gl/mapbox_gl.dart';


class MapaPage extends StatefulWidget {


  @override
  _MapaPageState createState() => _MapaPageState();
}

class _MapaPageState extends State<MapaPage> {
  MapboxMapController mapController;

  //coordenada
  final center = LatLng(-33.43749,-70.651024);

  

  void _onMapCreated(MapboxMapController controller) {
    mapController = controller;
    _onStyleLoaded();
  }
  void _onStyleLoaded() {
    addImageFromAsset("assetImage", "assets/custom-icon.png");
    addImageFromUrl("networkImage", "https://via.placeholder.com/50");
  }

   /// Adds an asset image to the currently displayed style
  Future<void> addImageFromAsset(String name, String assetName) async {
    final ByteData bytes = await rootBundle.load(assetName);
    final Uint8List list = bytes.buffer.asUint8List();
    return mapController.addImage(name, list);
  }

  /// Adds a network image to the currently displayed style
  Future<void> addImageFromUrl(String name, String url) async {
    var response = await http.get(url);
    return mapController.addImage(name, response.bodyBytes);
  }


  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:MapboxMap(   
          onMapCreated: _onMapCreated,
          initialCameraPosition:
          CameraPosition(
            target: center, 
            zoom: 15,
            ),
        ),
      floatingActionButton:FloatingActionButton(
          child: Icon( Icons.add ),
          onPressed: () {
            
            mapController.addSymbol( SymbolOptions(

              geometry: center,
              // iconSize: 3,
              iconImage: 'assetImage',
              textField: 'La Previa',
              textOffset: Offset(0, 5)
            ));

          }
        ),
        
   );
  }
}