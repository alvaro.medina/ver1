import 'package:flutter/material.dart';
import 'package:restaurant_app/widgets/appbar.dart';
import 'package:restaurant_app/widgets/cardHome.dart';

class ProfilePage extends StatefulWidget {
  static const route = '/profile';
  final AnimationController animationController;
  const ProfilePage({Key key, this.animationController}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage>
    with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(context),
      body: Center(
        child: Text('Aea'),
      ),
    );
  }
}
