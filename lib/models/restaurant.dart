class Restaurant {
  final String razonSocial;
  final String categoria;
  final String nombre;
  final String rating;
  final String ubicacion;
  final String photoURL;
  final String restaurantId;
  final String descripcion;

  /// {} Stands for named constructor, aka optional
  Restaurant({this.restaurantId,this.descripcion,this.razonSocial, this.categoria, this.nombre, this.rating, this.ubicacion, this.photoURL});
  ///Custom constructor for map given at Firestore
  Restaurant.fromJson(Map<String, dynamic> map, String id)
      : restaurantId = id,
        nombre = map['restaurante_nombre'],
        razonSocial = map['razon_social'],
        rating = map['restaurante_rating'],
        categoria = map['restaurante_categoria'],
        ubicacion = map['restaurante_ubicacion'],
        photoURL = map['restaurante_photo'],
        descripcion = map['restaurante_descripcion']
        ;
}

