class Food {
  final String name;
  final String description;
  final int price;
  final int quantity;
  final String pictureUrl;

  Food(this.name, this.price, this.quantity,{this.description, this.pictureUrl});

  //Food Function that Converts a MAP OBJECT into a FOOD CLASS. 
  Food.fromJson(Map<String, dynamic> map)
      : name = map['plato'],
        price = map['precio'],
        quantity = map['quantity'],
        description = map['description'],
        pictureUrl = map['picture_url'];

  //MAP function that converts into a MAP OBJECT the values of a OBJECT {with ""}
  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map["name"] = name;
    map["description"] = description;
    map["price"] = price;
    map["quantity"] = quantity;
    map["pictureUrl"] = pictureUrl;
    return map;
  }


  
}
