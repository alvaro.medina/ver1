import 'package:flutter/material.dart';

class ThemeChanger with ChangeNotifier {
  bool _darkTheme = false;
  bool _customTheme = false;

  //cambiar tema
  ThemeData _currentTheme;

  bool get darkTheme => this._darkTheme;
  bool get customTheme => this._customTheme;

  ThemeData get currentTheme => this._currentTheme;

  ThemeChanger(int theme) {
    switch (theme) {
      case 1:
        _darkTheme = false;
        _customTheme = false;
        _currentTheme = ThemeData.light().copyWith(
          primaryColor: Color(0xffec7633),
          backgroundColor: Color(0xfff1ad3d),
          accentColor: Colors.orange,
          textTheme: _lightTextTheme(ThemeData.light().textTheme),
          iconTheme: IconThemeData(
            color: Color(0xffec7633),
            size: 30.0,
          ),
          buttonTheme: ButtonThemeData(
            buttonColor: Color(0xffec7633),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
          ),
        );
        break;
      case 2:
        _darkTheme = true;
        _customTheme = false;
        _currentTheme = ThemeData.dark().copyWith(
          accentColor: Color.fromRGBO(241, 175, 9, 1),
          textTheme: _lightTextTheme(ThemeData.light().textTheme),
        );
        break;
      case 3:
        _darkTheme = false;
        _customTheme = true;
        _currentTheme = ThemeData.dark().copyWith(
          accentColor: Colors.orange,
          primaryColorLight: Colors.white,
          scaffoldBackgroundColor: Color.fromRGBO(137, 31, 31, 0.8),
          textTheme: _lightTextTheme(ThemeData.light().textTheme),
          /*
          textTheme: TextTheme(
            bodyText1: TextStyle(color: Colors.white),
          ),*/
        );
        break;

      default:
        _darkTheme = false;
        _customTheme = false;
        _currentTheme = ThemeData.light().copyWith(
          primaryColor: Color(0xffec7633),
          backgroundColor: Color(0xfff1ad3d),
          accentColor: Colors.orange,
          textTheme: _lightTextTheme(ThemeData.light().textTheme),
          iconTheme: IconThemeData(
            color: Color(0xffec7633),
            size: 30.0,
          ),
          buttonTheme: ButtonThemeData(
            buttonColor: Color(0xffec7633),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
          ),
        );
    }
  }

  set darkTheme(bool value) {
    _customTheme = false;
    _darkTheme = value;

    if (value) {
      _currentTheme = ThemeData.dark().copyWith(
        accentColor: Color.fromRGBO(241, 175, 9, 1),
      );
    } else {
      _currentTheme = ThemeData.light().copyWith(
        accentColor: Colors.orange,
      );
    }

    notifyListeners();
  }

  set customTheme(bool value) {
    _customTheme = value;
    _darkTheme = false;
    if (value) {
      _currentTheme = ThemeData.dark().copyWith(
        accentColor: Colors.orange,
        primaryColorLight: Colors.white,
        scaffoldBackgroundColor: Color.fromRGBO(137, 31, 31, 0.8),
        textTheme: TextTheme(
          bodyText1: TextStyle(color: Colors.white),
        ),
      );
    } else {
      _currentTheme = ThemeData.light().copyWith(
        accentColor: Colors.orange,
      );
    }
    notifyListeners();
  }

  ///APARTADO DE LAS LETRAS Y COLORES DE LETRA
  ///
  ////////////////////////////////////////////

  TextTheme _lightTextTheme(TextTheme base) {
    return base.copyWith(
      ///Texto OSCURO PARA TITULOS --- HEADLINE 1 ---
      headline1: base.headline1.copyWith(
          fontFamily: 'Montserrat',
          fontSize: 27.0,
          fontWeight: FontWeight.w600,
          letterSpacing: -0.5,
          color: Color(0xff323137)),

      ///Texto BLANCO PARA TITULOS --- HEADLINE 2 ---
      headline2: base.headline2.copyWith(
          fontFamily: 'Montserrat',
          fontSize: 23.0,
          fontWeight: FontWeight.w600,
          color: Color(0xfff4f5f7)),

      ///Texto GRIS PARA NAVEGADORES --- HEADLINE 3 ---
      headline3: base.headline3.copyWith(
          fontFamily: 'Montserrat',
          fontSize: 18.0,
          fontWeight: FontWeight.w500,
          color: Color(0xff5C626B)),

      ///Texto NARANJA PARA PRECIOS --- HEADLINE 4 ---
      headline5: base.headline5.copyWith(
          fontFamily: 'Montserrat',
          fontSize: 17.0,
          fontWeight: FontWeight.w600,
          color: Color(0xffcc8359)),

      ///Texto OSCURO PARA TITULOS DE CONTENIDO --- BODYTEXT1 ---
      bodyText1: base.bodyText1.copyWith(
          fontFamily: 'Montserrat',
          fontSize: 17.0,
          fontWeight: FontWeight.w600,
          color: Color(0xff323137)),

      ///Texto GRIS para descripciones de articulos -- BODYTEXT2 ---
      bodyText2: base.bodyText2.copyWith(
          fontFamily: 'Montserrat',
          fontSize: 15.0,
          fontWeight: FontWeight.w500,
          letterSpacing: 0.5,
          color: Color(0xff908ba9)),
    );
  }
}
