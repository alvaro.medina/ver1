import 'package:flutter/material.dart';

class BottomBar extends StatelessWidget {
  final int index;
  final Function setTab;
  const BottomBar({Key key, @required this.index, @required this.setTab})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: BottomAppBar(
          shape: CircularNotchedRectangle(),
          notchMargin: 6.0,
          color: Colors.transparent,
          elevation: 9.0,
          clipBehavior: Clip.antiAlias,
          child: Container(
              height: 50.0,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25.0),
                      topRight: Radius.circular(25.0)),
                  color: Colors.white),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                        height: 50.0,
                        width: MediaQuery.of(context).size.width / 2 - 40.0,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            IconButton(
                              icon: Icon(Icons.home),
                              color: index == 0
                                  ? Colors.orange
                                  : Color(0xFF676E79),
                              onPressed: () {
                                setTab(0);
                              },
                            ),
                            IconButton(
                              icon: Icon(Icons.person_outline),
                              color: index == 1
                                  ? Colors.orange
                                  : Color(0xFF676E79),
                              onPressed: () {
                                setTab(1);
                              },
                            )
                          ],
                        )),
                    Container(
                        height: 50.0,
                        width: MediaQuery.of(context).size.width / 2 - 40.0,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            IconButton(
                              icon: Icon(Icons.access_alarm),
                              color: index == 2
                                  ? Colors.orange
                                  : Color(0xFF676E79),
                              onPressed: () {
                                setTab(2);
                              },
                            ),
                            IconButton(
                              icon: Icon(Icons.person_outline),
                              color: index == 2
                                  ? Colors.orange
                                  : Color(0xFF676E79),
                              onPressed: () {
                                setTab(2);
                              },
                            )
                          ],
                        )),
                  ]))),
    );
  }
}
/*
class BottomBar extends StatefulWidget {
  final Function(String index) changeIndex;
  final String index;
  BottomBar({this.changeIndex, this.index});

  @override
  _BottomBarState createState() => _BottomBarState();
}

class _BottomBarState extends State<BottomBar> {
  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 6.0,
        color: Colors.transparent,
        elevation: 9.0,
        clipBehavior: Clip.antiAlias,
        child: Container(
            height: 50.0,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25.0),
                    topRight: Radius.circular(25.0)),
                color: Colors.white),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      height: 50.0,
                      width: MediaQuery.of(context).size.width / 2 - 40.0,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          IconButton(
                            icon: Icon(Icons.home),
                            color: widget.index == "/"
                                ? Colors.orange
                                : Color(0xFF676E79),
                            onPressed: () {
                              print("/");
                              widget.changeIndex("/");
                            },
                          ),
                          IconButton(
                            icon: Icon(Icons.person_outline),
                            color: widget.index == "/profile"
                                ? Colors.orange
                                : Color(0xFF676E79),
                            onPressed: () {
                              print("profile");
                              widget.changeIndex("/profile");
                            },
                          )
                        ],
                      )),
                  Container(
                      height: 50.0,
                      width: MediaQuery.of(context).size.width / 2 - 40.0,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          IconButton(
                            icon: Icon(Icons.access_alarm),
                            color: widget.index == "/principal"
                                ? Colors.orange
                                : Color(0xFF676E79),
                            onPressed: () {
                              print("principal");
                              widget.changeIndex("/principal");
                            },
                          ),
                          IconButton(
                            icon: Icon(Icons.person_outline),
                            color: widget.index == "/profile"
                                ? Colors.orange
                                : Color(0xFF676E79),
                            onPressed: () {
                              print("otro2");
                              widget.changeIndex("/profile");
                            },
                          )
                        ],
                      )),
                ])));
  }
}
*/
