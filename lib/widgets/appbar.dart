import 'package:flutter/material.dart';
import 'package:restaurant_app/services/auth_services.dart';
import 'package:provider/provider.dart';

Widget appBar(BuildContext context) {
  const AssetImage homeImage = AssetImage('assets/appbar2.png');

  return AppBar(
    backgroundColor: Colors.transparent,
   
    flexibleSpace: Stack(
      children: [
        Container(
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: FractionalOffset.topLeft,
                  end: FractionalOffset.topRight,
                  colors: [
                Color(0xffec7633).withOpacity(0.5),
                Color(0xfff1ad3d).withOpacity(0.6),
              ],
                  stops: [
                0.0,
                1.0
              ])),
        ),
      ],
    ),
    elevation: 0.08,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.vertical(
        bottom: Radius.circular(1),
      ),
    ),
    centerTitle: true,
    leading: IconButton(
      icon: Icon(Icons.menu, color: Colors.white),
      onPressed: () {
        context.read<AuthenticationService>().signOut();
      },
    ),
    title: Image.asset(
      'assets/sirVePE.png',
      fit: BoxFit.contain,
      height: 38.0,
    ),
    actions: <Widget>[
      IconButton(
        icon: Icon(Icons.exit_to_app, color: Colors.white),
        onPressed: () {
          context.read<AuthenticationService>().signOut();
        },
      ),
    ],
  );
}
